prefix = /usr
datadir = $(prefix)/share
sysconfdir = /etc
keyringdir = $(datadir)/keyrings
aptdir = $(sysconfdir)/apt/
apttrusteddir = $(aptdir)/trusted.gpg.d

GPG = gpg
INSTALL = install
INSTALL_DATA = $(INSTALL) -s -m644

KEYS = keys/*.asc
KEYRING = debian-pm-archive-keyring.gpg
APT_PREFERENCES = preferences

$(KEYRING): $(KEYS)
	mkdir -p -m700 gnupg
	keys='$^'; \
	for key in $$keys; do \
	$(GPG) --homedir gnupg --import $$key || exit 1; \
	done
	$(GPG) --homedir gnupg --export > $@

$(APT_PREFERENCES):
	./apt/pin-debian-pm-repository.sh

install: $(KEYRING) $(APT_PREFERENCES)
	mkdir -p $(DESTDIR)$(keyringdir) $(DESTDIR)$(apttrusteddir)
	$(INSTALL_DATA) $(KEYRING) $(DESTDIR)$(keyringdir)
	$(INSTALL_DATA) $(KEYRING) $(DESTDIR)$(apttrusteddir)
	$(INSTALL_DATA) $(APT_PREFERENCES) $(DESTDIR)$(aptdir)

clean:
	rm -f $(KEYRING)
	rm -rf gnupg
	rm -f $(APT_PREFERENCES)
